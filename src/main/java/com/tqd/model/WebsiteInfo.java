package com.tqd.model;

public class WebsiteInfo {

	private String host;
	private String url;
	private long navigateCount;
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public long getNavigateCount() {
		return navigateCount;
	}
	public void setNavigateCount(long navigateCount) {
		this.navigateCount = navigateCount;
	}
	
}
