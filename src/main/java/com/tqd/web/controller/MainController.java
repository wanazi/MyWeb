package com.tqd.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tqd.model.WebsiteInfo;

@Controller
@RequestMapping(value="/main")
public class MainController {

	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index(Model model){
		model.addAttribute("userName", "John");
		return "main";
	}
	
	@RequestMapping(value="/website/list",method=RequestMethod.GET)
	public String listWebsite(Model model){
		List<WebsiteInfo> websites=new ArrayList<>();
		WebsiteInfo wi=new WebsiteInfo();
		wi.setHost("www.baidu.com");
		wi.setUrl("http://www.baidu.com");
		wi.setNavigateCount(123L);
		websites.add(wi);
		
		wi=new WebsiteInfo();
		wi.setHost("www.qq.com");
		wi.setUrl("http://www.qq.com");
		wi.setNavigateCount(1233L);
		websites.add(wi);
		
		model.addAttribute("websites", websites);
		return "weblist";
	}
}
